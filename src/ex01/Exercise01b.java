package ex01;

/**
 * Created by chw4 on 1/05/2017.
 */
public class Exercise01b {
    public static void main(String[] args) {
        Exercise01b ex01b = new Exercise01b();
        ex01b.programStart();
    }

    public void programStart(){
        Thread myThread = new Thread(new MyRunnable());
        myThread.start();

        myThread.interrupt();
    }
}
