package ex01;

/**
 * Created by chw4 on 1/05/2017.
 */
public class Exercise01 {
    public static void main(String[] args) {
        Exercise01 ex01 = new Exercise01();
        ex01.programStart();
    }

    public void programStart(){
        Thread mythread = new Thread(new Runnable() {
            @Override
            public void run() {
                while (!Thread.currentThread().isInterrupted()) {
                }
            }
        });

        mythread.start();

        for (int i = 0; i < 1000000; i++) {
            System.out.println(i);
        }

        mythread.interrupt();

    }

}
