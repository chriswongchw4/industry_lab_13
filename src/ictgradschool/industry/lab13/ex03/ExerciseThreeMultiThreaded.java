package ictgradschool.industry.lab13.ex03;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Created by chw4 on 1/05/2017.
 */
public class ExerciseThreeMultiThreaded extends ExerciseThreeSingleThreaded {

    protected double estimatePI(long numSamples) {


        List<Thread> threads = new ArrayList<>();
        List<myRunnable> runnables = new ArrayList<>();

        for (long i = 0 ; i < numSamples; i ++){
            myRunnable r = new myRunnable();
            runnables.add(r);

            Thread t = new Thread(r);
            threads.add(t);
            t.start();

        }

        for (Thread t:threads){
            try {
                t.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        double numInsideCircle = 0;

        for (myRunnable r:runnables) {
            numInsideCircle +=r.inCircle;
        }

        double estimatedPi = 4.0 * numInsideCircle / (double) runnables.size();
        return estimatedPi;
    }



    public static void main(String[] args) {
        new ExerciseThreeMultiThreaded().start();
    }



    public class myRunnable implements Runnable{
        public double inCircle = 0;
        static final int INTERVAL = 100000;

        @Override
        public void run() {
            ThreadLocalRandom random = ThreadLocalRandom.current();
            for (long j = 0; j < INTERVAL; j++) {

                double x = random.nextDouble();
                double y = random.nextDouble();

                if (Math.pow(x, 2.0) + Math.pow(y, 2.0) < 1.0) {
                    inCircle++;
                }
            }
            inCircle = inCircle/INTERVAL;
        }


    }




}
