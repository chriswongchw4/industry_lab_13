package ictgradschool.industry.lab13.ex04;

import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;



public class ConcurrentBankingApp {

    public static void main(String[] args) {

        List<Transaction> transactions = TransactionGenerator.readDataFile();
        final BlockingQueue<Transaction> queue = new ArrayBlockingQueue<>(10);

        BankAccount account = new BankAccount();

        Thread consumer1 = new Thread(new Consumer(queue, account));
        Thread consumer2 = new Thread(new Consumer(queue, account));

        Thread producer = new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < transactions.size(); i++) {
                    try {
                        queue.put(transactions.get(i));
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }

               // consumer1.interrupt();
               // consumer2.interrupt();
            }
        });

        producer.start();
        consumer1.start();
        consumer2.start();

        try {
            producer.join();
            consumer1.join();
            consumer2.join();
        } catch (InterruptedException ie) {
            ie.printStackTrace();
        }

        System.out.println("Final balance: " + account.getFormattedBalance());
    }

    public static class Consumer implements Runnable {
        BlockingQueue<Transaction> queue;
        BankAccount account;

        public Consumer(BlockingQueue<Transaction> bq, BankAccount acct) {
            this.queue = bq;
            this.account = acct;
        }

        @Override
        public void run() {
            while (!queue.isEmpty() || !Thread.currentThread().isInterrupted()) {
                Transaction add = queue.poll();
                if (add != null) {
                    switch (add._type) {
                        case Deposit:
                            account.deposit(add._amountInCents);
                            break;
                        case Withdraw:
                            account.withdraw(add._amountInCents);
                            break;
                    }
                }
            }
        }
    }
}
