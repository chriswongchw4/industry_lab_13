package ex02;

import java.util.ArrayList;
import java.util.List;

public class ExerciseTwo {
    private int value = 0;
    private void start() throws InterruptedException {
        List<Thread> threads = new ArrayList<>();
        for (int i = 1; i <= 100; i++) {
            final int toAdd = i;
            Thread t = new Thread(new Runnable() {
                @Override
                public void run() {
                    add(toAdd);
                    try {
                        Thread.sleep(15);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            });
            t.start();
            threads.add(t);
        }
        for (Thread t : threads) {
            t.join();
        }
        System.out.println("value = " + value);
    }
    private void add(int i) {
        value = value + i;
    }
    public static void main(String[] args) throws InterruptedException {
        new ExerciseTwo().start();
    }
}

// 1. value = 5050
// 2. No. Add locks, or add synchronized to the method, or add Thread.sleep to give it enough time to process.