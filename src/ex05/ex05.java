package ex05;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by chw4 on 2/05/2017.
 */
public class ex05 {

    public static class PrimeFactorsTask implements Runnable {
        private long n;
        List<Long> primeFactors = new ArrayList<>();
        TaskState theStatus = TaskState.Initialized;

        public enum TaskState {Initialized, Completed, Aborted}



        public PrimeFactorsTask(long n) {
            this.n = n;
        }


        @Override
        public void run() {
            long number = n;
            for (long factor = 2; factor * factor <= n; factor++) {

                // if factor is a factor of n, repeatedly divide it out
                while (number % factor == 0) {
                    primeFactors.add(factor);
                    number = number / factor;
                }
            }

            if (number > 1) {
                primeFactors.add(number);
            }


            theStatus = TaskState.Completed;
        }

        public long n() {
            return n;
        }

        public List<Long> getPrimeFactors() throws IllegalStateException {

            return this.primeFactors;
        }


        public TaskState getState() {
            return this.theStatus;
        }

    }

    public static void main(String[] args) {


    }
}
